# AVRUtilty
If you want something like the Arduino platform, but 
with more flexibility, this is for you.
Uses avr-g++.

#### Example

    #include <avr/io.h>
    #include <util/delay.h>
    #include <stdlib.h>
    #include <stdio.h>

    #include "uart.h"

    int main()
    {
	Uart::initialise();
	Uart::init_stdio();
	while(1)
	{
	    printf("does it work?\n");
	    _delay_ms(1000);
	}
	return 0;
    }

