#include "uart.h"

void Uart::initialise()
{
    /*Set baud rate to 9600 */
    unsigned ubrr = 16000000UL/16/9600;
    UBRR0H = (unsigned char)(ubrr>>8);
    UBRR0L = (unsigned char)ubrr;
    /*Enable receiver and transmitter */
    UCSR0B = (1<<RXEN0)|(1<<TXEN0);
    UCSR0A = (1<<UDRE0);
    /* Set frame format: 8data, 1stop bit */
    UCSR0C =  (1 << UCSZ01) | (1 << UCSZ00);
}

int Uart::put_char(char c, FILE *stream)
{
    while (( UCSR0A & (1<<UDRE0)) == 0 ) {};
    
    UDR0 = c;
    return 0;
}

void Uart::init_stdio()
{
    static FILE out;
    fdev_setup_stream(&out,
    		      Uart::put_char,
    		      NULL,
    		      _FDEV_SETUP_WRITE);
    stdout = &out;
}
