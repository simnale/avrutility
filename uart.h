#ifndef UART_H
#define UART_H

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>

namespace Uart
{
    void initialise();
    int put_char(char c, FILE *stream);
    void init_stdio();
}

#endif
